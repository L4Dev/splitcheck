﻿using System;

public class People
{
    private string name;
    private float total;
    private float dispo;
    public People(string _name)
    {
        name = _name;
        total = 0;
        dispo = 0;
    }
    public void addToTotal(float price)
    { total += price; }
    public void setDispo(float _dispo)
    {
        dispo = _dispo;
    }
    public float getTotal()
    {
        return total;
    }
    public float money()
    {
        return dispo - total;
    }
}
